#include "sheet.h"

#include "sheet.png.h"

sf::Texture *getSheet() {
    static sf::Texture *instance = new sf::Texture;
    static bool loaded = false;

    if (!loaded) {
        instance->loadFromMemory(sheet_png, sheet_png_len);
        instance->setSmooth(false);
        loaded = true;
    }

    return instance;
}

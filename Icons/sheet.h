#ifndef __SHEET_H
#define __SHEET_H

#include <SFML/Graphics.hpp>

const sf::IntRect ICON_HOME(16 * 0, 0, 16, 16);
const sf::IntRect ICON_INFO(16 * 1, 0, 16, 16);
const sf::IntRect ICON_MUSIC(16 * 2, 0, 16, 16);
const sf::IntRect ICON_NEXT(16 * 3, 0, 16, 16);
const sf::IntRect ICON_PAUSE(16 * 4, 0, 16, 16);
const sf::IntRect ICON_PLAY(16 * 5, 0, 16, 16);
const sf::IntRect ICON_PREV(16 * 0, 16, 16, 16);

sf::Texture *getSheet();

#endif

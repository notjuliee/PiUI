#ifndef __SMOLRUI_BUTTON_H
#define __SMOLRUI_BUTTON_H

#include <string>
#include "Element.h"
#include "Label.h"

namespace sui {

class Button : public Element {
   public:
    Button(Element* child);

    void setChild(Element* child);

    void setColor(sf::Color c);

    virtual void render(sf::RenderWindow& win);

    virtual void setPosition(int x, int y);

    virtual const sf::FloatRect getBounds() const;

   private:
    Element* m_child;
    sf::RectangleShape m_background;
};

}  // namespace sui

#endif

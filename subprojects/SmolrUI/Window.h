#ifndef __SMOLRUI_WINDOW_H
#define __SMOLRUI_WINDOW_H

#include <SFML/Graphics.hpp>
#include <functional>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
#include "Element.h"

namespace sui {

class Window {
   public:
    Window();

    void run();
    void addElement(Element *el, const std::string &scene = "main");

    void setScene(const std::string &scene);

    void setInterval(const std::function<void()> &f, int interval);

    sf::Vector2u getSize() const;

   private:
    sf::RenderWindow m_win;

    std::string m_scene = "main";

    std::unordered_map<std::string, std::vector<Element *>> m_elements;
    std::vector<std::tuple<int, const std::function<void()> *, int>>
        m_intervals;
};

};  // namespace sui

#endif

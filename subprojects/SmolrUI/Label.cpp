#include "Label.h"

#include <iostream>
#include "Font.h"

namespace sui {

Label::Label() {
    m_label.setFont(*getFont());
    setColor(sf::Color::Green);
    setSize(17);
    setText("Label");
}

void Label::setText(const std::string& label) {
    m_label.setString(label.c_str());
}

void Label::setSize(int size) { m_label.setCharacterSize(size); }

void Label::setColor(sf::Color color) { m_label.setFillColor(color); }

void Label::render(sf::RenderWindow& win) { win.draw(m_label); }

void Label::setPosition(int x, int y) { m_label.setPosition(x, y); }

const sf::FloatRect Label::getBounds() const {
    sf::FloatRect b = m_label.getGlobalBounds();
    b.height = m_label.getCharacterSize() + 5;
    return b;
}

};  // namespace sui

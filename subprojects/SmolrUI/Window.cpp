#include "Window.h"

#include <SFML/Graphics.hpp>

namespace sui {

Window::Window() : m_win(sf::VideoMode(320, 240), "SmolUI") {
    m_win.setFramerateLimit(30);
}

void Window::setScene(const std::string &scene) { m_scene = scene; }

void Window::addElement(Element *el, const std::string &scene) {
    m_elements[scene].push_back(el);
}

void Window::setInterval(const std::function<void()> &f, int interval) {
    m_intervals.emplace_back(interval, new std::function<void()>(f), 0);
    f();
}

void Window::run() {
    sf::Clock c;

    while (m_win.isOpen()) {
        sf::Event event;
        while (m_win.pollEvent(event)) {
            if (event.type == sf::Event::Closed) m_win.close();

            if (event.type == sf::Event::MouseButtonPressed) {
                int x = event.mouseButton.x, y = event.mouseButton.y;

                for (auto el : m_elements[m_scene]) {
                    sf::FloatRect b = el->getBounds();
                    if (b.contains(x, y)) el->onClick();
                }
            }
        }

        int millis = c.getElapsedTime().asMilliseconds();

        for (auto &i : m_intervals) {
            if (millis - std::get<2>(i) >= std::get<0>(i)) {
                (*std::get<1>(i))();
                std::get<2>(i) = millis;
            }
        }

        m_win.clear(sf::Color::Black);
        for (auto el : m_elements[m_scene]) {
            el->render(m_win);
        }
        m_win.display();
    }
}

sf::Vector2u Window::getSize() const { return m_win.getSize(); }

};  // namespace sui

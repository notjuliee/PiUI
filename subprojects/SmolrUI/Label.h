#ifndef __SMOLRUI_LABEL_H
#define __SMOLRUI_LABEL_H

#include <SFML/Graphics.hpp>
#include "Element.h"

namespace sui {

class Label : public Element {
   public:
    Label();

    void setText(const std::string& label);
    void setSize(int size);
    void setColor(sf::Color color);

    virtual void render(sf::RenderWindow& win);

    virtual void setPosition(int x, int y);

    virtual const sf::FloatRect getBounds() const;

   private:
    sf::Text m_label;
};

}  // namespace sui

#endif

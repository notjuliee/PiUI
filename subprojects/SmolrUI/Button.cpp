#include "Button.h"

#include <iostream>
#include "Font.h"

namespace sui {

Button::Button(Element* child) {
    setChild(child);
    setColor(sf::Color::Black);
    setPosition(0, 0);
}

void Button::setChild(Element* child) {
    m_child = child;

    sf::FloatRect cs = m_child->getBounds();
    m_background.setSize(sf::Vector2f(cs.width + 10, cs.height + 10));
    sf::Vector2f p = m_background.getPosition();
    m_child->setPosition(p.x + 5, p.y + 5);
}

void Button::setColor(sf::Color c) { m_background.setFillColor(c); }

void Button::render(sf::RenderWindow& win) {
    win.draw(m_background);
    m_child->render(win);
}

void Button::setPosition(int x, int y) {
    m_background.setPosition(x, y);
    m_child->setPosition(x + 5, y + 5);
}

const sf::FloatRect Button::getBounds() const {
    return m_background.getGlobalBounds();
}

};  // namespace sui

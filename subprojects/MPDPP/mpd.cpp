#include "mpd.h"

#include <mpd/client.h>
#include <stdexcept>

namespace mpd {

// Class Song
Song::Song(struct mpd_song* s) { m_song = s; }
Song::~Song() { mpd_song_free(m_song); }

const char* Song::getTag(Tag tag) {
    const char* t = mpd_song_get_tag(m_song, (mpd_tag_type)tag, 0);
    return t ? t : "UNKNOWN";
}

// Class Status
Status::Status(struct mpd_status* s) { m_status = s; }
Status::~Status() { mpd_status_free(m_status); }

bool Status::isPlaying() {
    return mpd_status_get_state(m_status) == MPD_STATE_PLAY;
}

// Class Connection
Connection::Connection(const char* host, int port, int timeout) {
    m_conn = mpd_connection_new(host, port, timeout);
}
Connection::~Connection() { mpd_connection_free(m_conn); }

// Getters
Song Connection::getCurrentSong() {
    struct mpd_song* s = mpd_run_current_song(m_conn);
    if (mpd_connection_get_error(m_conn) != MPD_ERROR_SUCCESS)
        throw std::runtime_error(mpd_connection_get_error_message(m_conn));
    return Song(s);
};

Status Connection::getStatus() {
    struct mpd_status* s = mpd_run_status(m_conn);
    if (mpd_connection_get_error(m_conn) != MPD_ERROR_SUCCESS)
        throw std::runtime_error(mpd_connection_get_error_message(m_conn));
    return Status(s);
}

// Control
bool Connection::playPause() { return mpd_run_toggle_pause(m_conn); }
bool Connection::prev() { return mpd_run_previous(m_conn); }
bool Connection::next() { return mpd_run_next(m_conn); }

};  // namespace mpd
